Documentazione progetto Futura - Guida tecnica
==============================================

Il presente repository contiene la documentazione tecnica necessaria all'installazione e alla configurazione dell'applicativo Futura per la compilazione del PEI e del PDP.

La documentazione è scritta in `reStructuredText <http://www.sphinx-doc.org/rest.html>`_ ed il rendering in HTML, PDF ed ePUB è disponibilie su `Read The Docs <https://futura-tech-guide.readthedocs.io/it/latest/>`_.

Contribuisci
------------

Chiunque può contribuire a migliorare la documentazione.

Licenze
-------

.. image:: https://img.shields.io/badge/code-AGPLv3-blue.svg
    :target: https://www.gnu.org/licenses/agpl-3.0.en.html
    :alt: Code AGPLv3

.. image:: https://img.shields.io/badge/documentation-CC%20BY--SA-lightgrey.svg
    :target: https://creativecommons.org/licenses/by-sa/4.0/
    :alt: Documentation CC BY-SA

