﻿futura-tech-guide: Indice
=========================

Il presente manuale raccoglie le informazioni necessarie all'installazione e alla configurazione dell'applicazione Futura.

.. toctree::
	:maxdepth: 2
	
	installazione
